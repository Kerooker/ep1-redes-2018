package jogo.da.velha.cliente

import jogo.da.velha.servidor.SERVER_PORT
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.net.Socket
import java.util.*

suspend fun main(args: Array<String>) {

    val tcpSocket = Socket("127.0.0.1", SERVER_PORT)
    val udpSocket = DatagramSocket()

    val tcpInput = tcpSocket.getInputStream()
    val tcpOutput = tcpSocket.getOutputStream()
    val tcpScanner = Scanner(tcpInput)

    GlobalScope.launch {
        while (true) {
            if (tcpScanner.hasNextLine()) {
                println(tcpScanner.nextLine())
            } else if (tcpScanner.hasNext()) {
                print(tcpScanner.next())
            }
        }
    }

    GlobalScope.launch {
        val sysScanner = Scanner(System.`in`)
        while(true) {
            delay(100)
            if (sysScanner.hasNextLine()) {
                val nextLine = sysScanner.nextLine()
                val trimmedLine = nextLine.replace(" ", "")
                if(trimmedLine.length == 2 && trimmedLine[0].isDigit() && trimmedLine[1].isDigit()) {
                    //Send a TCP package with this
                    tcpOutput.write("$trimmedLine\n".toByteArray())
                    tcpOutput.flush()
                }else {
                    val bytes = "$nextLine\n".toByteArray()
                    val packet = DatagramPacket(bytes, bytes.size, InetAddress.getByName("127.0.0.1"), SERVER_PORT + 1)
                    udpSocket.send(packet)

                }
            }
        }
    }

    runBlocking {
        //Keeping main thread alive
        delay(Long.MAX_VALUE)
    }
}