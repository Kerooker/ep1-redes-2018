package jogo.da.velha.servidor.game

import jogo.da.velha.servidor.game.TicTacToeGame.Board.GameState.*

class TicTacToeGame {

    var nextToPlay = PlayerToken.values().random()
    var board = Board()
    val gameState
        get() = board.calculateGameState()

    fun play(lineNumber: Int, columnNumber: Int) {
        checkValidPosition(lineNumber, columnNumber)
        checkPositionIsUnnocupied(lineNumber, columnNumber)

        placeOnBoard(lineNumber, columnNumber)
    }

    private fun checkValidPosition(lineNumber: Int, columnNumber: Int) {
        if (lineNumber !in (0..2)) throw InvalidPositionException()
        if (columnNumber !in (0..2)) throw InvalidPositionException()
    }

    private fun checkPositionIsUnnocupied(lineNumber: Int, columnNumber: Int) {
        if (!board.isAvailableSpot(lineNumber, columnNumber)) throw OccupiedPositionException()
    }

    private fun placeOnBoard(lineNumber: Int, columnNumber: Int) {
        board.placeOnSpot(lineNumber, columnNumber, nextToPlay)
        invertNextToPlay()
    }

    private fun invertNextToPlay() {
        nextToPlay = when(nextToPlay) {
            PlayerToken.X -> PlayerToken.O
            PlayerToken.O -> PlayerToken.X
        }
    }

    class Board {
        private val matrix = arrayOf(
            arrayOf<PlayerToken?>(null, null, null),
            arrayOf<PlayerToken?>(null, null, null),
            arrayOf<PlayerToken?>(null, null, null)
        )

        fun isAvailableSpot(lineNumber: Int, columnNumber: Int): Boolean {
            return matrix[lineNumber][columnNumber] == null
        }

        fun placeOnSpot(lineNumber: Int, columnNumber: Int, token: PlayerToken) {
            matrix[lineNumber][columnNumber] = token
        }

        fun calculateGameState(): GameState {
            if(boardIsAWin()) return calculateWinner()
            if(boardIsADraw()) return DRAW
            return PLAYING
        }

        private fun boardIsAWin(): Boolean {
            return (isColumnWin() || isRowWin() || isDiagWin())
        }

        private fun isColumnWin(): Boolean {
            for(column in 0..2) {
                val firstToken = matrix[0][column]
                val secondToken = matrix[1][column]
                val thirdToken = matrix[2][column]

                if(firstToken == null) continue
                if(firstToken == secondToken && firstToken == thirdToken)return true
            }
            return false
        }

        private fun isRowWin(): Boolean {
            for(line in 0..2) {
                val firstToken = matrix[line][0]
                val secondToken = matrix[line][1]
                val thirdToken = matrix[line][2]

                if(firstToken == null)continue
                if (firstToken == secondToken && firstToken == thirdToken)return true
            }
            return false
        }

        private fun isDiagWin(): Boolean {
            return isForwardDiagWin() || isInverseDiagWin()
        }

        private fun isForwardDiagWin(): Boolean {
            val firstToken = matrix[0][0]
            val secondToken = matrix[1][1]
            val thirdToken = matrix[2][2]

            if (firstToken == null) return false
            return firstToken == secondToken && firstToken == thirdToken
        }

        private fun isInverseDiagWin(): Boolean {
            val firstToken = matrix[0][2]
            val secondToken = matrix[1][1]
            val thirdToken = matrix[2][0]
            if (firstToken == null) return false
            return firstToken == secondToken && firstToken == thirdToken
        }

        private fun calculateWinner(): GameState {
            for(line in 0..2) {
                val firstToken = matrix[line][0]
                val secondToken = matrix[line][1]
                val thirdToken = matrix[line][2]

                if(firstToken == null)continue
                if (firstToken == secondToken && firstToken == thirdToken)return WON(firstToken)
            }

            for(column in 0..2) {
                val firstToken = matrix[0][column]
                val secondToken = matrix[1][column]
                val thirdToken = matrix[2][column]

                if(firstToken == null) continue
                if(firstToken == secondToken && firstToken == thirdToken)return WON(firstToken)
            }

            return WON(matrix[1][1]!!)
        }

        private fun boardIsADraw(): Boolean {
            return matrix.flatten().none { it == null }
        }

        sealed class GameState {
            object PLAYING : GameState()
            object DRAW : GameState()
            data class WON(val winner: PlayerToken) : GameState()
        }

        override fun toString(): String {
            return with(matrix) {
                """
                    |${this[0][0].string}|${this[0][1].string}|${this[0][2].string}|
                    |${this[1][0].string}|${this[1][1].string}|${this[1][2].string}|
                    |${this[2][0].string}|${this[2][1].string}|${this[2][2].string}|

                """.trimIndent()
            }
        }

        private val PlayerToken?.string: String
        get() {
            return when(this) {
                PlayerToken.X -> this.token
                PlayerToken.O -> this.token
                null -> "_"
            }
        }
    }

    private class InvalidPositionException : RuntimeException("A posição selecionada é inválida! Linhas e colunas precisam estar no intervalo [0,2].")
    private class OccupiedPositionException : RuntimeException("A posição selecionada é inválida! Escolha um lugar disponível.")

    enum class PlayerToken(val token: String) {
        X("X"),
        O("O")
    }

}