package jogo.da.velha.servidor.socket

import jogo.da.velha.servidor.client.handler.GameHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.DatagramPacket
import java.net.DatagramSocket

class ChatServer(private val socket: DatagramSocket) {

    private var receivedData = ByteArray(1024)

    fun start() {
        GlobalScope.launch {
            while(true) {
                val packet = DatagramPacket(receivedData, receivedData.size)
                socket.receive(packet)

                val message = String(packet.data).filterNot { it == '\u0000' }

                // Send to everyone as a TCP package
                GameHandler.messageEveryone(message)
                receivedData = ByteArray(1024)


            }
        }
    }
}