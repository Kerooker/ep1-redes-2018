package jogo.da.velha.servidor.socket

import jogo.da.velha.servidor.client.handler.ClientHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.ServerSocket
import java.net.Socket

class TicTacToeServer(private val socket: ServerSocket) {

    fun start() {
        GlobalScope.launch {
            println("Servidor inicializado.")
            while (true) {
                val client = socket.accept()

                println("Client connected: ${client.inetAddress.hostAddress}")
                handleClientConnection(client)
            }
        }
    }

    private fun handleClientConnection(client: Socket) {
        GlobalScope.launch {
            ClientHandler.handle(client)
        }
    }

}