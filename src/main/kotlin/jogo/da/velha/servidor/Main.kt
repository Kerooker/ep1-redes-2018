package jogo.da.velha.servidor

import jogo.da.velha.servidor.socket.ChatServer
import jogo.da.velha.servidor.socket.TicTacToeServer
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.net.DatagramSocket
import java.net.ServerSocket

var SERVER_PORT = 8080

fun main(args: Array<String>) {
    if (args.size == 1) {
        SERVER_PORT = args[0].toInt()
    }

    printServerStarting()

    startServerSocket()
}

private fun printServerStarting() {
    println("Servidor TCP inicializando na porta $SERVER_PORT")
    println("Servidor UDP inicializando na porta ${SERVER_PORT + 1}")
}

private fun startServerSocket() {
    val server = ServerSocket(SERVER_PORT)
    val chatServer = DatagramSocket(SERVER_PORT + 1)

    TicTacToeServer(server).start()
    ChatServer(chatServer).start()

    runBlocking {
        // Keeping main thread alive
        delay(Long.MAX_VALUE)
    }
}

