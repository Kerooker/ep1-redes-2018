package jogo.da.velha.servidor.client.handler.thread

import jogo.da.velha.servidor.client.handler.GameHandler
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.Socket

class PlayerThread(client: Socket) : MessageableClient(client) {

    init {
        GlobalScope.launch {
            while(true) {
                if(reader.hasNextLine()) {
                    handleReaderLine()
                }
           }
        }
    }

    private fun handleReaderLine() {
        val line = reader.nextLine().replace(" ", "")

        if (line.length != 2) {
            sendMessage("Não entendi! Por favor, digite apenas o número de linha seguido do número de coluna!")
        }
        val lineNumber = line[0].toString().toInt()
        val columnNumber = line[1].toString().toInt()

        if (GameHandler.currentPlayer != this) {
            sendMessage("Não é sua vez de jogar! Aguarde o outro jogador.")
            return
        }

        try {
            GameHandler.executePlay(lineNumber, columnNumber)
        }catch (ex: Exception) {
            GameHandler.currentPlayer?.sendMessage(ex.message ?: "Erro desconhecido")
        }



    }

}