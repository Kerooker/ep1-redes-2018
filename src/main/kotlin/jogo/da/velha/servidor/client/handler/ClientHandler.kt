package jogo.da.velha.servidor.client.handler

import jogo.da.velha.servidor.client.handler.thread.PlayerThread
import jogo.da.velha.servidor.client.handler.thread.WatcherThread
import java.net.Socket

object ClientHandler {

    var playerOne: PlayerThread? = null
    var playerTwo: PlayerThread? = null
    var watchers: MutableList<WatcherThread> = mutableListOf()

    fun handle(client: Socket) {
        if(playerOne == null) {
            createPlayerOne(client)
            return
        }

        if (playerTwo == null) {
            createPlayerTwo(client)
            return
        }

        createWatcher(client)
    }

    private fun createPlayerOne(socket: Socket) {
        playerOne = PlayerThread(socket)
        sendStartinMessageToPlayerOne()
    }

    private fun sendStartinMessageToPlayerOne() {
        playerOne?.sendMessage("Bem vindo! Você será o Jogador 1 desta partida. Aguarde conexão do jogador 2\n")
    }

    private fun createPlayerTwo(socket: Socket) {
        playerTwo = PlayerThread(socket)
        sendStartingMessageToPlayer2()
        warnPlayer1OfPlayer2Entrance()
        startGame()
    }

    private fun sendStartingMessageToPlayer2() {
        playerTwo?.sendMessage("Bem vindo! Você será o Jogador 2 desta partida. O jogo irá começar!\n")
    }

    private fun warnPlayer1OfPlayer2Entrance() {
        playerOne?.sendMessage("O jogador 2 acabou de se conectar. O jogo irá começar!\n")
    }

    private fun createWatcher(client: Socket) {
        val watcher = WatcherThread(client)
        watchers.add(watcher)
        sendStartingMessageToWatcher(watcher)
    }

    private fun sendStartingMessageToWatcher(watcherThread: WatcherThread) {
        watcherThread.sendMessage("Bem vindo! O servidor já está cheio. Você irá assistir e poderá participar do chat!\n")
    }

    private fun startGame() {
        println("Começando o jogo!")
        GameHandler.startGame()
    }

}