package jogo.da.velha.servidor.client.handler

import jogo.da.velha.servidor.game.TicTacToeGame
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

object GameHandler {

    var game = TicTacToeGame()
    var currentPlayer = ClientHandler.playerOne

    fun startGame() {
        GlobalScope.launch {
            messageBothPlayers("Inicializando o jogo. O Jogador 1 jogará de ${game.nextToPlay.token}, e começará agora!\n")
            messageBothPlayers(game.board.toString())
            messageCurrentPlayerTurn()
        }
    }

    private fun messageCurrentPlayerTurn() {
        currentPlayer?.sendMessage("É sua vez de jogar! Escolha digite uma linha (0-2) e uma coluna (0-2) para selecionar a posição!")
        currentPlayer?.sendMessage("Por exemplo: \"1 1 \" jogará no meio!")
    }

    fun executePlay(line: Int, column: Int) {
        game.play(line, column)
        messagePlayerAction()
        switchActivePlayer()
        messageEveryone(game.board.toString())

        if(game.gameState is TicTacToeGame.Board.GameState.DRAW) {
            messageEveryone("Esse jogo empatou!")
            restartGame()
        }else if (game.gameState is TicTacToeGame.Board.GameState.WON) {
            messageEveryone("UaU! O jogador que jogou com ${(game.gameState as TicTacToeGame.Board.GameState.WON).winner} ganhou!")
            restartGame()
        }
    }

    private fun messagePlayerAction() {
        if (currentPlayer == ClientHandler.playerOne) {
            messageEveryone("O jogador 1 acabou de fazer uma jogada!")
        } else {
            messageEveryone("O jogador 2 acabou de fazer uma jogada!")
        }
    }

    private fun switchActivePlayer() {
        currentPlayer = if(currentPlayer == ClientHandler.playerOne) {
            ClientHandler.playerTwo
        }else {
            ClientHandler.playerOne
        }
        messageCurrentPlayerTurn()
    }

    private fun restartGame() {
        game = TicTacToeGame()
        currentPlayer = ClientHandler.playerOne
        startGame()
    }

    private fun messageBothPlayers(message: String) {
        ClientHandler.playerOne?.sendMessage(message)
        ClientHandler.playerTwo?.sendMessage(message)
    }

    fun messageEveryone(message: String) {
        messageBothPlayers(message)
        messageSpectators(message)
    }

    private fun messageSpectators(message: String) {
        ClientHandler.watchers.forEach {
            try {
                it.sendMessage(message)
            }catch(_: Throwable){
                ClientHandler.watchers.remove(it)
            }
        }
    }

}