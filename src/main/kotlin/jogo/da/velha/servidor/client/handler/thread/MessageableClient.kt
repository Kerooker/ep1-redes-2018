package jogo.da.velha.servidor.client.handler.thread

import java.io.OutputStream
import java.net.Socket
import java.util.*

abstract class MessageableClient(client: Socket) {

    protected val reader = Scanner(client.getInputStream())
    private val writer = client.getOutputStream()


    fun sendMessage(string: String) {
        writer.write("$string\n")
        writer.flush()
    }

    private fun OutputStream.write(string: String) {
        return write(string.toByteArray())
    }

}